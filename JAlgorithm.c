#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int vertex = 4; // vertex number
int **w;    // adjacency matrix
int **w_reweight; //adjancency matrix after reweight
int *d;       // 紀錄起點到各個點的最短路徑長度
int *parent;  // 紀錄各個點在最短路徑樹上的parent是誰
int *visit;  // 紀錄各個點是不是已在最短路徑樹之中
int *h; //weight of vertex
int **shortest_path = {0};

void create_graph();
void bellman_ford(int source);
int negative_cycle();
void reweight();
void dijkstra(int source);
void find_path(int source, int x, int destination);

void main()
{
    int a, b;
    srand(6);
    create_graph();    
    bellman_ford(vertex);    //O(VE); 
    if(negative_cycle()){
        printf("n cycle");
        return;
    }

    for(a = 0; a < vertex; a++){
        printf("   h%d = %d\n", a, h[a]);
    }

    reweight(); //O(V^2)

    free(parent);
    parent = malloc(sizeof(int) * vertex);

    printf("\n\nShortest Path");
    fflush(stdout);
    for(a = 0; a < vertex; a++){
        dijkstra(a);
        for(b = 0; b < vertex; b++){
            find_path(a, b, b);
            printf("path %d to %d = %d\n", a, b, shortest_path[a][b]);
        }
    }
}

void create_graph(){

    int a, b;

    w = malloc(sizeof(int*) * (vertex+1));
    w_reweight = malloc(sizeof(int*) * vertex);
    d = malloc(sizeof(int) * (vertex+1));
    parent = malloc(sizeof(int) * (vertex+1));
    visit = malloc(sizeof(int) * vertex);
    h = malloc(sizeof(int) * (vertex+1));
    shortest_path = malloc(sizeof(int*) * (vertex));

    for(a = 0; a < (vertex + 1); a++)
       w[a] = malloc(sizeof(int) * (vertex+1));
    for(a = 0; a < vertex; a++){
        shortest_path[a] = malloc(sizeof(int) * vertex);
        w_reweight[a] = malloc(sizeof(int) * vertex);
    }


    for(a = 0; a < vertex; a++){
        for(b = 0; b < vertex ;b++){
            if(rand() % 2){
                w[a][b] = 1e9;
            }
            else if(a == b)
                w[a][b] = rand()%10+1;
            else{
                if(rand() % 6)
                    w[a][b] = rand()%10+1;
                else
                    w[a][b] = -rand()%10+1;
            }
        }
    }

    for(a = 0; a < vertex; a++){
        w[vertex][a] = 0;
        printf("[%d][%d]\n", vertex,a);
        w[a][vertex] = 1e9;
    }

    for(a=0; a<vertex+1; a++){
        for(b = 0; b < vertex+1; b++){
            if(w[a][b] != 1e9)
                printf("%4d", w[a][b]);
            else
                printf("   X");
        }
        printf("\n");
    }
}
void bellman_ford(int source)
{
    for (int i = 0; i < vertex+1; i++)
        d[i] = 1e9;
 
    d[source] = 0;              // 設定起點的最短路徑長度
    parent[source] = source;    // 設定起點是樹根（父親為自己）
 
    for (int i=0; i < vertex; i++)   // 重覆步驟V-1次
        for (int a=0; a < vertex+1; ++a) // 全部的邊都當作捷徑
            for (int b=0; b < vertex+1; ++b)
                if (d[a] != 1e9 && w[a][b] != 1e9)
                    if (d[a] + w[a][b] < d[b])
                    {
                        d[b] = d[a] + w[a][b];
                        printf("a=%d, b=%d, d[%d]=%d, w[%d][%d]=%d\n", a, b, b, d[b], a, b, w[a][b]);
                        parent[b] = a;
                        h[b] =d[b];
                    }
}

int negative_cycle()
{
    int a, b;
    for (a = 0; a < (vertex + 1); ++a)
        for (b = 0; b < (vertex + 1); ++b)
            if (d[a] != 1e9 && w[a][b] != 1e9)
                if (d[a] + w[a][b] < d[b])
                    return 1;
    return 0;
}


void reweight()
{
    int a, b;

    for(a = 0; a < vertex; a++){
        for(b = 0; b < vertex; b++){
            
            if(w[a][b]!=1e9){
                w_reweight[a][b] = w[a][b] + h[a] - h[b];
                printf("%4d", w_reweight[a][b]);
            }
            else{
                w_reweight[a][b] = 1e9;
                printf("    ");
            }
        }
        printf("\n");
    }
}

void dijkstra(int source)
{
    int i, k;

    for (i = 0; i < vertex; i++)
        visit[i] = 0;   // initialize
    for (i = 0; i < vertex; i++)
        d[i] = 1e9;
 
    d[source] = 0;
    parent[source] = source;
    for (k = 0; k < vertex; k++)
    {
        int a = -1, b = -1, min = 1e9;

        for (i = 0; i < vertex; i++)
            if (!visit[i] && d[i] < min)
            {
                a = i;  // 記錄這一條邊
                min = d[i];
            }
 
        if (a == -1) break;     // 起點有連通的最短路徑都已找完

        visit[a] = 1;
 
        for (b = 0; b < vertex; b++)
            if (!visit[b] && d[a] + w_reweight[a][b] < d[b])
            {
                d[b] = d[a] + w_reweight[a][b];
                parent[b] = a;
            }
    }
}
 
void find_path(int source, int x, int destination)   // 印出由起點到x點的最短路徑
{
    if (x != parent[x]){ // 先把之前的路徑都印出來
        shortest_path[source][destination] = shortest_path[source][destination] + w[parent[x]][x];
        find_path(source, parent[x], destination);
    }
    else if(source != x)
        shortest_path[source][destination] = 1e9;
    printf("%d\n",x);
}
